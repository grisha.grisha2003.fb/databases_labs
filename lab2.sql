USE Interpol_DB;
INSERT INTO Crimer
(FirstName, SecondName, NickName, Height, HairColor, EyesColor, SpecialSigns, Citizenship, BirthDate, BirthPlace, LastLivingPlace)
VALUES ('Иван','Иванов','Иваныч', 178, 'Кислотный Жёлтый', 'Карие', 'Отсутствуют передние зубы', 'Россия', '1975-01-01', 'Москва', 'Москва'),
	   ('Пётр','Петров','Петрович', 190, 'Яркий синий' , 'Голубые' , ' Тату Адидас ', 'Россия' , '1969-03-08', 'Санкт-Питербург', 'Санкт-Питербург'),
       ('Питер','Паркер','Человек_Паук', 175, 'Каштановый', 'Карие', 'Лазает по стенам', 'США', '1999-02-23', 'Los Angeles', 'Los Angeles'),
       ('Тони','Старк','Железный_Человек', 183,  'Чёрный', 'Карие', 'Он железный', 'США', '1975-09-12', 'Los Angeles', 'Los Angeles'),
       ('Григорий','Переверзев','МЕНЯНЕОТЧИСЛЯТ', 165, 'Каштановый', 'Голубые', 'Скоро утонет в долгах', 'Россия', '2003-05-17', 'Уссурийск', 'Калининград');


INSERT INTO CrimeCase
(Name, OpenDate, CloseDate, Description)
VALUES ('Крупное ограбление ларька', '2005-06-05', '2005-06-06', 'Ларёк был ограблен. Крупно'),
	   ('Драка за гаражами', '2011-01-01', '2012-01-01', 'На месте преступления найдено 2 зуба'),
       ('Крупные поставки самогона в страны 3го мира', '2000-04-22', '2023-03-03', 'Большая часть времени ушла на ликвидацию вещдока'),
       ('Двойной перевод', '2023-02-15', '2023-11-15', 'Было ли это ошибкой?'),
       ('Перевёл старушку через дорогу', '2021-06-04', '2021-06-04', 'А это как тут оказалось?');
       
INSERT INTO Languages
(Lang)
VALUES('eng'),('rus'),('fra'),('deu'),('pol');

INSERT INTO CriminalProfession
(ProfessionName)
VALUES ('Взломщик'), ('Контрабандист'), ('Информатор'), ('Алкоголик'), ('Должник'), ('Студент');

INSERT INTO Crimer_Languages
(CrimerID, LanguageID)
SELECT Crimer.ID, languages.ID
FROM crimer, languages
WHERE crimer.FirstName = 'Григорий' AND languages.Lang = 'rus';

INSERT INTO Crimer_Languages
(CrimerID, LanguageID)
SELECT Crimer.ID, languages.ID
FROM crimer, languages
WHERE crimer.Citizenship = 'США' AND languages.Lang = 'eng';

INSERT INTO crimer_criminalprofession
(CrimerID, CriminalProfessionID)
SELECT Crimer.ID, criminalprofession.ID
FROM crimer, criminalprofession
WHERE crimer.EyesColor = 'Карие' AND criminalprofession.ProfessionName = 'Алкоголик';

INSERT INTO crimer_crimecase
(CrimerID, CrimeCaseID, CrimerStatus)
SELECT Crimer.ID, crimecase.ID , 'Участник'
FROM crimer, crimecase
WHERE crimer.BirthPlace = 'Los Angeles' AND crimecase.Name = 'Крупное ограбление ларька';

INSERT INTO crimer_crimecase
(CrimerID, CrimeCaseID, CrimerStatus)
SELECT Crimer.ID, crimecase.ID , 'Участник'
FROM crimer, crimecase
WHERE crimer.NickName = 'Человек_Паук' AND crimecase.Name = 'Перевёл старушку через дорогу';

UPDATE crimecase
SET Name = 'Битва за гаражами'
WHERE ID = 2;

INSERT INTO crimer_crimecase
(CrimerID, CrimeCaseID, CrimerStatus)
VALUES (1, 2, 'Свидетель'),
	   (3, 2, 'Свидетель'),
       (2, 3, 'Участник'),
       (4, 3, 'Участник'),
       (5, 3, 'Свидетель'),
       (5, 4, 'Участник');
       
